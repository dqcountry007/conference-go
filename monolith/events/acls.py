import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_picture(entry):
    # url is set to the url for the api, has a query= at the end + entry for what the user is searching for
    url = "https://api.pexels.com/v1/search?query=" + entry
    # Authorization is required to use Pexels API
    #   All requests you make to API require your key.
    #       This is provided by adding an Authorization header.
    headers = {"Authorization": PEXELS_API_KEY}
    # response = request to get things back from the Pexels API
    # saying (url, headers={"Authorization": PEXELS_API_KEY}) or (url, headers=headers)
    response = requests.get(url, headers=headers)
    # set response = json.loads (parses JSON string into Python dictionary), we want to do this with the content we get back from Pexels API
    response = json.loads(response.content)
    # set photos equal to the dictionary's value at a key named "photos", the value is a list of photos
    photos = response["photos"]
    # if there are photos return them
    if photos and len(photos) > 0:
        return photos


# other example of what get_city_picture is referencing
# def get_city_picture(search_term):
#     url = "https://api.pexels.com/v1/search?query=" + search_term
#     dog = {"Authorization": PEXELS_API_KEY}
#     response = requests.get(url, headers=dog)
#     data = json.loads(response.content)
#     photos = data["photos"]
#     return photos


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US&limit=5&appid="
        + OPEN_WEATHER_API_KEY
    )

    response = requests.get(url)

    data = json.loads(response.content)[0]

    lon = data["lon"]
    lat = data["lat"]

    return {}

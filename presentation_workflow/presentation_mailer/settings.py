# strip django project down to its bare minimum, which
# allows it to run faster
DEBUG = True
ALLOWED_HOSTS = []
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

EMAIL_HOST = "mail"
EMAIL_SUBJECT_PREFIX = ""
